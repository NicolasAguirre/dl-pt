const { currencies } = require('../models')
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args))

exports.convert = async (amount, currency_from_id, currency_to_id) => {
  const currency_from_updated_at = await currencies.findOne(
  { 
    where: { id: currency_from_id },
    attributes: ['updatedAt']
  })
  
  // check last currency rate update
  const today = new Date().toDateString()
  const rate_last_updated_at = new Date(currency_from_updated_at.dataValues.updatedAt).toDateString()
  
  // always get latest currency rate update, maximum once a day
  // this logic is because the technical test specified 'Asumir que este servicio tiene un coste por llamado.' to minimize costs.
  if(today.valueOf() != rate_last_updated_at.valueOf()) {
    // update currencies then return conversion
    await updateCurrencies()
  }
  
  const currency_from = await currencies.findOne({ where: { id: currency_from_id } })
  const currency_to = await currencies.findOne({ where: { id: currency_to_id } })

  // to convert currency_from dolar based rate, devide the dolar based rate from the currency you want to convert to a specific amount between the dolar based rate from the currency you want to convert from.
  const converted_currency_dolar_based_rate = currency_to.dolarBasedRate / currency_from.dolarBasedRate
  const converted_amount = converted_currency_dolar_based_rate * amount

  return converted_amount
}

const updateCurrencies = async () => {
  // call fixer api, get eur, uyu, usd base usd.
  const dateString = new Date().toISOString().split('T')[0]
  
  const requestOptions = {
    method: 'GET',
    redirect: 'follow',
    headers: { 'apikey': 'GnwsUfEmtBOdcrXOTh1fBuGxuTvU8qCH' }
  }
  
  fetch(`https://api.apilayer.com/fixer/${dateString}?symbols=eur%2Cuyu%2Cusd&base=usd`, requestOptions)
  .then(response => response.text())
  .then(result => {
    result = JSON.parse(result)
    Object.entries(result["rates"]).forEach(async ([key, value]) => {
      // for each currency on rates api response, update currencies stored in the database
      await currencies.update({ dolarBasedRate: value }, {
        where: {
          code: key
        }
      })
    })
  })
  .catch(error => {
    console.log('error', error)
  })
}