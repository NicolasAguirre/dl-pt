const { transactions } = require('../models')
const { accounts } = require('../models')
const currency = require('./currency')

exports.transfer = async (account_from_id, account_to_id, amount, date, description, res) => {
  const account_from = await accounts.findOne({ where: { id: account_from_id } })
  const account_to = await accounts.findOne({ where: { id: account_to_id } })
  var amount_to_receive = amount
  var tax = 0.0

  if (account_to.dataValues.userId != account_from.dataValues.userId) {
    // add 1% to tax
    tax = amount * 0.01
  }

  if (account_to.dataValues.currencyId != account_from.dataValues.currencyId) {
    // convert amount to receive 
    amount_to_receive = await currency.convert(amount, account_from.dataValues.currencyId, account_to.dataValues.currencyId)
  }

  if (account_from.capital < amount + tax) {
    res.send("the account doesn't have enough capital to complete the transfer.")
  } else {
    // transfer money
    await transactions.create({
      accountFromId: account_from_id,
      accountToId: account_to_id,
      value: amount,
      tax: tax
    })
    // update accounts
    await accounts.update({ capital: account_to.dataValues.capital + amount_to_receive }, { where: { id: account_to_id } })
    await accounts.update({ capital: account_from.dataValues.capital - ( amount + tax ) }, { where: { id: account_from_id } })

    res.send("transfer completed.")
  }
}