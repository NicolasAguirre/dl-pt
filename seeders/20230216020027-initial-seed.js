'use strict'
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'currencies',
      [
        {
          id: 1,
          name: "PESOS URUGUAYOS",
          code: "UYU",
          dolarBasedRate: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 2,
          name: "DOLARES AMERICANOS",
          code: "USD",
          dolarBasedRate: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 3,
          name: "EUROS",
          code: "EUR",
          dolarBasedRate: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ],
      {}
    )

    await queryInterface.bulkInsert(
      'users',
      [
        {
          id: 1,
          name: 'user1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 2,
          name: 'user2',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 3,
          name: 'user3',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    )

    await queryInterface.bulkInsert(
      'accounts',
      [
        // accounts for user 1
        {
          id: 1,
          userId: 1,
          currencyId: 1,
          number: 144235,
          capital: 10,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 2,
          userId: 1,
          currencyId: 2,
          number: 144235,
          capital: 10,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 3,
          userId: 1,
          currencyId: 3,
          number: 144235,
          capital: 10,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        // accounts for user 2
        {
          id: 4,
          userId: 2,
          currencyId: 1,
          number: 423511,
          capital: 10,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 5,
          userId: 2,
          currencyId: 2,
          number: 423511,
          capital: 10,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 6,
          userId: 2,
          currencyId: 3,
          number: 423511,
          capital: 10,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        // accounts for user 3
        {
          id: 7,
          userId: 3,
          currencyId: 1,
          number: 546389,
          capital: 10,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 8,
          userId: 3,
          currencyId: 2,
          number: 546389,
          capital: 10,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 9,
          userId: 3,
          currencyId: 3,
          number: 546389,
          capital: 10,
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ],
      {}
    )
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('accounts', null, {})
    await queryInterface.bulkDelete('users', null, {})
    await queryInterface.bulkDelete('currencies', null, {})
  }
}
