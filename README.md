
# Getting started

**DISCLAIMER** This project is built for PostgreSQL database. The current configuration is located in the `config` folder. Please update the `config.js` file to connect to your empty database, or create a new empty PostgreSQL database with matching configurations (username, password, dbname, etc.).

**Install all packages**

`npm install` 

**Migrations** To run migrations, execute the following command:

`npm run migrate` 

To rollback migrations, execute the following command:

`npm run migrate:undo` 

**Seeds** To run seeds, execute the following command:

`npm run seeds` 

To rollback seeds, execute the following command:

`npm run seeds:undo` 

**Start the project**

```
npm run dev
```

**Curl request**
The following curl request will transfer 1 USD to a UYU account of the same user 
```
curl --location --request POST 'http://localhost:3000/transfer' \

--header 'Content-Type: application/json' \

--data-raw '{

"account_from_id": 5,

"account_to_id": 4,

"amount": 1,

"date": "Fri Feb 02 2023 10:00:00 GMT+0200 (CEST)",

"description": "this is a description"

}'
```
