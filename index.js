const express = require('express')
const transfer = require('./routes/transaction')
const app = express()
const port = process.env.PORT || 3000

app.use(express.json());
app.use('/transfer', transfer)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
