const express = require('express')
const transfer = require('../controllers/transaction')
const router = express.Router()

router.post('/', async (req, res) => {
    const { account_from_id, account_to_id, amount, date, description } = req.body
    await transfer.transfer(account_from_id, account_to_id, amount, date, description, res)
})

module.exports = router