'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class transactions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  transactions.init({
    accountFromId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'accounts',
        key: 'id'
      }
    },
    accountToId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'accounts',
        key: 'id'
      }
    },
    value: DataTypes.FLOAT,
    description: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    tax: DataTypes.FLOAT,
    description: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'transactions',
  })
  return transactions
}